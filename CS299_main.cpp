#include "CS299_arr.h"
//Please place your name here:
//
//
using namespace std;

int main()
{
    node * array[SIZE] = {0,0,0,0,0};
    build(array);
    display_all(array);

    //PLEASE PUT YOUR CODE HERE to call the functions
    cout << endl << endl;
    cout << "Test begins: *********************************" << endl;
    //int result = find_longest(array, SIZE);
    //int result = remove_last(array, SIZE);
    //int result = add_last(array, SIZE);
    int result = remove_all(array, SIZE);
    cout << result << endl;


    cout << "Test ends: *********************************" << endl;
    cout << endl << endl;
    

    display_all(array);
    destroy(array);
    
    return 0;
}
